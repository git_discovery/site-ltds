import {defineConfig} from "vitepress";

export default defineConfig({
	lang: 'zh-cn',
	titleTemplate: '好好住',
	description: '千万屋主装修经验攻略',
	themeConfig: {
		siteTitle: '好好住',
		nav: nav(),
		footer: {
			message: '家宅一生（北京）科技有限公司京ICP备15059464号-4京公网安备11010502047643号',
			copyright: '版权所有 2014-2022 好好住'
		},
	}
})
function nav(){
	return [
		{ text: '首页', link: '/'},
		{ text: '发现', link: '/discovery'},
		{ text: '整屋案例', link: '/example'},
		{ text: '设计师入住', link: '/design'},
		{ text: '营造家奖', link: '/stimulate'},
		{ text: '品牌入住', link: '/brand'},
		{
			text: '关于我们',
			items: [
				{text: '关于我们', link: '/about'},
				{text: '加入我们', link: '/join'},
				{text: '联系我们', link: '/concat'},
				{text: '用户协议', link: '/arreement'},
				{text: '隐私声明', link: '/private'},
			]
		},
	]
}