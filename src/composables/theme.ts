import {useColorMode} from '@vueuse/core'

export const theme = useColorMode()

