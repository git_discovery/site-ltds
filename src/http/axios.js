import axios from "axios";

const BASE_URL = import.meta.env.BASE_URL
const instance = axios.create({
    baseURL: BASE_URL
})