import {createApp} from 'vue'
import App from './App.vue'
import './styles/element.css'
import  './styles/vars.css'
import  './styles/base.css'
import  './styles/theme.css'

import router from "./router";

createApp(App)
	.use(router)
	.mount('#app')
