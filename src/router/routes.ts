import {RouteRecordRaw} from "vue-router";

const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		component: ()=> import('@/views/Home.vue')
	},{
		path: '/discovery',
		component: ()=> import('@/views/Discovery.vue')
	},{
		path: '/example',
		component: ()=> import('@/views/Example.vue')
	},{
		path: '/design',
		component: ()=> import('@/views/Design.vue')
	},{
		path: '/brand',
		component: ()=> import('@/views/Brand.vue')
	},{
		path: '/about',
		components: {
			default: ()=> import('@/views/team/AboutUs.vue'),
			join: ()=> import('@/views/team/JoinUs.vue'),
			concat: ()=> import('@/views/team/Concat.vue'),
		}
	},{
		path: '/clause',
		redirect: '/clause/arreement',
		children: [
			{
				path: '/',
				component: ()=> import('@/views/clause/Arreement.vue'),
			},{
				path: '/clause/private',
				component: ()=> import('@/views/clause/Private.vue')
			}
		]
	}
]

export default routes